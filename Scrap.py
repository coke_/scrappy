#!/usr/bin/python3.4
print('Importing libraries')

from urllib.request import urlopen
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET

print('Done.')



#settings
forum_url = 'http://knoppix.net/forum/forum.php'		
forum_name = 'knoppix'
subforums_limit = 2
threads_limit = 3	
subforums_count = 0
threads_count = 0




def url_to_dom(url):
	"""Takes URL and returns BeautifulSoup object(DOM)"""
	print('Downloading from %s' % (url))
	
	data = urlopen(url).read()
	
	print('Done.\nBuilding DOM.')
	
	dom = BeautifulSoup(data, "html.parser")
	
	print('Done.')
	return dom

	
#XML root element
XMLroot = ET.Element('FORUM')
XMLroot.attrib['name'] = forum_name

forum = url_to_dom(forum_url)

#iterate through subforum anchors
for subforum_anchor in forum.select('#c_cat3 .forumtitle > a'):

	if subforums_count == subforums_limit:
		print('Subforum limit reached.')
		break
	subforums_count+=1
	
	#append subforum to xml tree
	print('Appending %s subforum to the XML tree' % (subforum_anchor.get_text()))
	XMLsubforum = ET.SubElement(XMLroot, "SUBFORUM")
	XMLsubforum.attrib['name'] = subforum_anchor.get_text()
	print('Done.')
	
	#open subforum (hrefs are relative -> make them absolute)
	subforum = url_to_dom('http://knoppix.net/forum/%s' % (subforum_anchor.get('href')))
	
	#iterate through subforum threads anchors
	for thread_anchor in subforum.select('#threads > li .threadtitle > a'):
	
		if threads_count == threads_limit:
			print('Thread limit reached.')
			#counter reset
			threads_count=0
			break
		threads_count+=1
		
		#open thread (hrefs are relative -> make them absolute)
		thread = url_to_dom('http://knoppix.net/forum/%s' % (thread_anchor.get('href')))
		
		print('Appending %s thread to the XML tree' % (thread_anchor.get_text()))
		XMLthread = ET.SubElement(XMLsubforum, 'THREAD')
		XMLthread.attrib['name'] = thread.select('#pagetitle a')[0].get_text()
		print('Done.')
		
		for post in thread.select('#posts > li'):
			XMLpost = ET.SubElement(XMLthread, 'POST')
			XMLpost.attrib['id'] = post.select('.postcounter')[0].get_text()

			XMLauthor = ET.SubElement(XMLpost, 'AUTHOR')

			XMLusername = ET.SubElement(XMLauthor, 'USERNAME')
			XMLusername.text = post.select('.username_container strong')[0].get_text()

			XMLtitle = ET.SubElement(XMLauthor, 'TITLE')
			XMLtitle.text = post.select('.usertitle')[0].get_text().strip()

			if len(post.select('.rank')):
				XMLrank = ET.SubElement(XMLauthor, 'RANK')
				XMLrank.text = post.select('.rank')[0].get_text()

			XMLjoin_date = ET.SubElement(XMLauthor, 'JOIN_DATE')
			XMLjoin_date.text = post.select('.userinfo_extra > dd')[0].get_text()

			XMLlocation = ET.SubElement(XMLauthor, 'LOCATION')
			XMLlocation.text = post.select('.userinfo_extra > dd')[1].get_text()

			#XMLposts = ET.SubElement(XMLauthor, 'POSTS')
			#XMLposts.text = post.select('.userinfo_extra > dd')[2].get_text()

			XMLtimestamp = ET.SubElement(XMLpost, 'TIMESTAMP')
			XMLtimestamp.text = post.select('.postdate > .date')[0].get_text()	

			XMLcontent = ET.SubElement(XMLpost, 'CONTENT')
			XMLcontent.text = post.select('.postcontent')[0].get_text()

			for attachment in post.select('.attachments > fieldset > ul > li'):
				XMLattachment = ET.SubElement(XMLpost, 'ATTACHMENT')
				XMLattachment.attrib['name'] = attachment.select('a')[0].get_text()
				XMLattachment.attrib['href'] = attachment.select('a')[0].get('href')


print('Writing to %s.xml' % (forum_name))
ET.ElementTree(XMLroot).write('%s.xml' % (forum_name), encoding='utf-8')
print('Done.')
